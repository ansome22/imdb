# Introduction

The plan for this project is to find trends from IMDB's dataset


# Current planned tests

* Amount movies each year
* Amount tv shows each year
* Amount of the same genres through each year
* Average time a TV show ends
* Amount of Adult rating shows through each year


# Requirements

1. To start this project you will need Python language

2. Along with the following python libaries 
    * Jupyter
    * numpy
    * matplot


# Resources 

["IMDB Dataset"](https://www.imdb.com/interfaces/)